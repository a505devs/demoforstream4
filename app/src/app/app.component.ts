import {Component, OnInit} from '@angular/core';
import {Message} from './message';
import {ChatService} from './chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  name = 'Bruce Wayne';
  messages: Message[] = [];

  constructor(private service: ChatService) {
  }

  send($event: Message): void {
    this.service.sendMessage($event);
  }

  ngOnInit(): void {
    this.service.messages$.subscribe(message => {
      this.messages.push(message);
    });
  }
}
