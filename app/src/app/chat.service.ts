import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import {Message} from './message';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  connection: signalR.HubConnection;

  private messageSubject = new Subject<Message>();

  get messages$(): Observable<Message> {
    return this.messageSubject.asObservable();
  }

  constructor() {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl('http://8a5a3e6d23bd.ngrok.io/chat')
      .build();

    this.connection.on('receiveMessage', (username: string, message: string) => {
      this.messageSubject.next({ name: username, message });
    });

    this.connection.start().catch(err => {
      console.error(err);
    });
  }

  sendMessage(message: Message): Promise<void> {
    return this.connection.send('sendMessage', message.name, message.message);
  }
}
