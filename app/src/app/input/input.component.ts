import {Component, Input, EventEmitter, Output} from '@angular/core';
import {Message} from '../message';
import {ControlValueAccessor} from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent {
  @Input() name = 'Test';

  message: string;

  @Output() messageSent = new EventEmitter<Message>();

  send(): void {
    this.messageSent.emit({name: this.name, message: this.message});
  }
}
